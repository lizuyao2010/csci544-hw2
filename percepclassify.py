#!/usr/bin/env python3
#cat pos.dev.txt | python3 percepclassify.py pos.per > pos.dev.out
#cat sentiment_dev.txt | python3 percepclassify.py sentiment.per > sentiment.dev.out
import json,sys
from perceplearn import add,product,argmax_dot_product
from collections import defaultdict

if __name__=="__main__":
    # read model file
    modelfile_name=sys.argv[1]
    with open(modelfile_name,'r',encoding='utf-8') as fr:
        old_weights,labels=json.load(fr)
        weights=defaultdict(lambda:defaultdict(int))
        for key in old_weights:
            for key2 in old_weights[key]:
                weights[key][key2]=old_weights[key][key2]
        labels=set(labels)
    total=0
    error=0
    # read test file from stdin, label each line
    for line in sys.stdin:
        line=line.strip().split()
        label=line[0]
        features=line[1:]
        pre_label=argmax_dot_product(features,weights,labels)
        print(pre_label)
        if label!=pre_label:
            error+=1
        total+=1
    print('accuracy:',1-error/float(total),file=sys.stderr)