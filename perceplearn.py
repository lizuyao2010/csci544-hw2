#!/usr/bin/env python3

#python3 perceplearn.py pos.train.txt pos.per -d pos.dev.txt
#python3 perceplearn.py sentiment_train.txt sentiment.per -d sentiment_dev.txt
import sys,json,math,random,argparse,copy
from collections import defaultdict

def add(weights,label,features,sign):
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        weights[feature][label]+=sign

def product(weights,label,features):    
    sum_val=0
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        sum_val+=weights[feature][label]
    return sum_val

def add_weights(averaged_weights,weights,features,n):
    for feature in features:
        if feature not in averaged_weights:
            averaged_weights[feature]=defaultdict(float)

        for label in labels:
            averaged_weights[feature][label]+=weights[feature][label]*n


def normalize_weights(averaged_weights,n):
    for feature in averaged_weights:
        for label in weights[feature]:
            averaged_weights[feature][label]/=float(n)

def argmax_dot_product(features,weights,labels):
    max_label=""
    max_val=float('-infinity')
    for label in labels:
        val=product(weights,label,features)
        if val>max_val:
            max_label=label
            max_val=val
    return max_label


def train(examples):
    global n
    global c
    error=0
    total=len(examples)
    for (i,example) in enumerate(examples):
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,weights,labels)
        if label!=pre_label:
            error+=1
            add(weights,label,features,1)
            add(weights,pre_label,features,-1)
            add(averaged_weights,label,features,n*total-c)
            add(averaged_weights,pre_label,features,-(n*total-c))
        c+=1
        #add_weights(averaged_weights,weights,features,labels,total-i+total*remain)
    error_rate=float(error)/float(total)
    return error_rate 

def predicate(examples):
    error=0
    total=len(examples)
    for example in examples:
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,averaged_weights,labels)
        if label!=pre_label:
            error+=1
    error_rate=float(error)/float(total)
    return error_rate 


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("TRAININGFILE", help="provide TRAININGFILE")
    parser.add_argument("MODEL", help="provide MODEL")
    parser.add_argument("-d","--DEVFILE", help="provide DEVFILE")
    args = parser.parse_args()

    weights=dict()
    averaged_weights=dict()
    train_examples=[]
    labels=set()

    # read train_file, construct examples list
    trainfile_name=args.TRAININGFILE
    with open(trainfile_name,'r',encoding='utf-8') as fp:
        for line in fp:
            line=line.strip().split()
            if line[0] not in labels:
                labels.add(line[0])
            train_examples.append(line)

    # read dev_file
    dev_examples=[]
    if args.DEVFILE:
        dev_file=args.DEVFILE
        with open(dev_file,'r',encoding='utf-8') as fp:
            for line in fp:
                line=line.strip().split()
                if line[0] not in labels:
                    labels.add(line[0])
                dev_examples.append(line)

    n=40
    pre_error_rate=0
    best_weights=dict()
    c=0
    for i in range(n):
        random.shuffle(train_examples)
        print("number of iteration:",i+1,file=sys.stderr)

        train_error_rate=train(train_examples)
        print("train accuracy:",1-train_error_rate,file=sys.stderr)
        if len(dev_examples)!=0:
            dev_error_rate=predicate(dev_examples)
            print("dev accuracy:",1-dev_error_rate,file=sys.stderr)

            if i==0:
                best_weights=copy.deepcopy(averaged_weights)
                best_error_rate=dev_error_rate
            else:
                if best_error_rate>dev_error_rate:
                    best_error_rate=dev_error_rate
                    best_weights=copy.deepcopy(averaged_weights)

    if len(dev_examples)==0:
        best_weights=copy.deepcopy(averaged_weights)

    modelfile_name=args.MODEL
    with open(modelfile_name,'w') as fw:
        json.dump([best_weights,list(labels)],fw)
