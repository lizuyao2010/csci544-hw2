#The accuracy of my part-of-speech tagger
    The accuracy of my part-of-speech tagger(percep) is 96.5%


#The precision, recall and F-score for each of the named entity types for my named entity recognizer, and the overall F-score
    processed 52923 tokens with 4351 phrases; found: 4264 phrases; correct: 3245.
    accuracy:  95.90%; precision:  76.10%; recall:  74.58%; FB1:  75.33
                  LOC: precision:  69.02%; recall:  77.44%; FB1:  72.99  1104
                 MISC: precision:  57.81%; recall:  49.89%; FB1:  53.56  384
                  ORG: precision:  77.53%; recall:  73.06%; FB1:  75.23  1602
                  PER: precision:  86.80%; recall:  83.39%; FB1:  85.06  1174

#My Naive Bayes classifier performance metrics on named entity recognization
    processed 52923 tokens with 4351 phrases; found: 16055 phrases; correct: 2392.
    accuracy:  72.08%; precision:  14.90%; recall:  54.98%; FB1:  23.44
                  LOC: precision:  13.37%; recall:  67.68%; FB1:  22.33  4980
                 MISC: precision:   2.20%; recall:  21.57%; FB1:   3.99  4364
                  ORG: precision:  20.96%; recall:  50.76%; FB1:  29.67  4117
                  PER: precision:  29.57%; recall:  62.77%; FB1:  40.20  2594

Because we assume features are independent when using naive bayes classifier. However, features not independent, for example previous tag have influence on current tag.