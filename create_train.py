#!/usr/bin/env python3

#python3 nblearn.py pos.train
import sys

# read train_file
trainfile_name=sys.argv[1]
with open(trainfile_name,'r',encoding='utf-8') as fp:
    for line in fp:
        pairs=line.strip().split()
        for pair in pairs:
            word,tag=pair.split('/')
            print(tag,word)