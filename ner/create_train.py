#!/usr/bin/env python3
# python3 create_train.py megam_train.txt > nb_train.txt
import json,collections,sys


with open(sys.argv[1],'r',encoding='utf-8') as fr:
    for line in fr:
        line=line.strip().split()
        label=line[0]
        bag=collections.Counter(line[1:])
        print (json.dumps([label,bag]))