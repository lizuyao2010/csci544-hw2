#!/usr/bin/env python3

#cat ner.esp.dev | python3 netag.py ner.model > test.ner.out
#python3 netag.py ner.model ner.esp.dev > test.ner.out
import sys,os,re
words=[]
newlines=set()
with open('test_file','w',encoding='utf-8') as fw:
    index=-1
    with open(sys.argv[2],'r',encoding='utf-8',errors='ignore') as fstdin:
        for line in fstdin:
            line=line.strip()
            if '/' not in line:
                sentence=[word+'/<unknown>/<unknown>' for word in line.split()]
            else:
                sentence=line.split()
            sentence.insert(0,'<bos>/<bos>/<bos>')
            sentence.append('<eos>/<eos>/<eos>')
            n=len(sentence)
            for (i,triple) in enumerate(sentence[1:n-1],1):
                word,tag,ner=re.match(r"(\S+)?/(\S+)/(\S+)",triple).groups('')
                p1_word,p1_tag,p1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-1]).groups('')
                n1_word,n1_tag,n1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i+1]).groups('')
                print(ner,'p1_w_'+p1_word,'p1_t_'+p1_tag,'c_w_'+word,'c_t_'+tag,'n1_w_'+n1_word,'n1_t_'+n1_tag,file=fw)
                words.append([word,tag,ner])
                index+=1
            newlines.add(index)
os.system('./megam.opt -nc -predict '+sys.argv[1]+' multitron test_file >test.out')
with open('test.out','r',encoding='utf-8') as fr, open('ner.output.txt','w',encoding="utf-8") as fw:
    for (i,predicate) in enumerate(fr):
        predicate=predicate.strip().split()
        pre_ner=predicate[0]
        true_ner=words[i][2]
        print('/'.join(words[i][:2]+[pre_ner]),end=' ',file=sys.stdout)
        print(' '.join(words[i]+[pre_ner]),file=fw)
        if i in newlines:
            print(file=sys.stdout)