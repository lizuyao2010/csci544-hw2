#!/usr/bin/env python3

#python3 convert2nbfile.py ner.esp.train ner.esp.dev
import sys,os,re

# read train_file
trainfile_name=sys.argv[1]
with open(trainfile_name,'r',encoding='utf-8',errors='ignore') as fp, open('nb_train.txt','w',encoding='utf-8') as fw:
    for line in fp:
        # parse sentence
        sentence=line.strip().split()
        sentence.insert(0,'<bos>/<bos>/<bos>')
        sentence.append('<eos>/<eos>/<eos>')
        n=len(sentence)
        for (i,triple) in enumerate(sentence[1:n-1],1):
            word,tag,ner=re.match(r"(\S+)?/(\S+)/(\S+)",triple).groups('')
            p1_word,p1_tag,p1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-1]).groups('')
            n1_word,n1_tag,n1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i+1]).groups('')
            print(ner,'p1_w_'+p1_word,'p1_t_'+p1_tag,'c_w_'+word,'c_t_'+tag,'n1_w_'+n1_word,'n1_t_'+n1_tag,file=fw)
#model=sys.argv[2]
#os.system("./megam.opt -nc multitron megam_train.txt > "+model)
dev_file=sys.argv[2]
with open(dev_file,'r',encoding='utf-8',errors='ignore') as fp, open('nb_dev.txt','w',encoding='utf-8') as fw:
    for line in fp:
        # parse sentence
        sentence=line.strip().split()
        sentence.insert(0,'<bos>/<bos>/<bos>')
        sentence.append('<eos>/<eos>/<eos>')
        n=len(sentence)
        for (i,triple) in enumerate(sentence[1:n-1],1):
            word,tag,ner=re.match(r"(\S+)?/(\S+)/(\S+)",triple).groups('')
            p1_word,p1_tag,p1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-1]).groups('')
            n1_word,n1_tag,n1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i+1]).groups('')
            print(ner,'p1_w_'+p1_word,'p1_t_'+p1_tag,'c_w_'+word,'c_t_'+tag,'n1_w_'+n1_word,'n1_t_'+n1_tag,file=fw)