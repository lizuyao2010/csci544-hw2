#!/usr/bin/env python3
#cat ner.esp.dev | python3 netag.py ner.model  > test.ner.out
#cat ner.esp.test | python3 netag.py ner.model  > ner.esp.test.out
import json,sys,re,codecs
from nelearn import add,product,argmax_dot_product,get_feature
from collections import defaultdict

total=0
error=0

def predicate_sentence(line,labels,fw):
    # parse sentence
    line=line.strip()
    if '/' not in line:
        sentence=[word+'/<unknown>/<unknown>' for word in line.split()]
    else:
        sentence=line.split()
    sentence.insert(0,'<bos>/<bos>/<bos>')
    sentence.insert(0,'<bos>/<bos>/<bos>')
    sentence.append('<eos>/<eos>/<eos>')
    n=len(sentence)
    p1_ner='<bos>'
    p2_ner='<bos>'
    for (i,triple) in enumerate(sentence[2:n-1],2):
        '''
        word,tag,ner=re.match(r"(\S+)?/(\S+)/(\S+)",triple).groups('')
        p2_word,p2_tag=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-2]).groups('')[:2]
        p1_word,p1_tag=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-1]).groups('')[:2]
        n1_word,n1_tag=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i+1]).groups('')[:2]
        '''
        
        word,tag=re.match(r"(\S+)?/(\S+)",triple).groups('')
        p2_word,p2_tag=re.match(r"(\S+)?/(\S+)",sentence[i-2]).groups('')[:2]
        p1_word,p1_tag=re.match(r"(\S+)?/(\S+)",sentence[i-1]).groups('')[:2]
        n1_word,n1_tag=re.match(r"(\S+)?/(\S+)",sentence[i+1]).groups('')[:2]
        
        features=['1_'+p1_ner,'2_'+p1_tag,'3_'+word,'4_'+tag,'5_'+n1_word,'6_'+n1_tag,'8_'+p1_word,'12_'+p2_tag,'13_'+p2_ner]
        if word:
            features.append('7_'+get_feature(word))
        if len(word)>0:
            features.append('11_'+word[-1:])
            if len(word)>1:
                features.append('9_'+word[-2:])
                if len(word)>2:
                    features.append('10_'+word[-3:])
        if len(word)>0:
            features.append('14_'+word[:1])
            if len(word)>1:
                features.append('15_'+word[:2])
                if len(word)>2:
                    features.append('16_'+word[:3])
        # predicate label
        pre_ner=argmax_dot_product(features,weights,labels)
        p2_ner=p1_ner
        p1_ner=pre_ner
        print('/'.join([word,tag,pre_ner]),end=' ',file=sys.stdout)
        '''
        print(' '.join([word,tag,ner,pre_ner]),file=fw)
        if ner!=pre_ner:
            global error
            error+=1
        global total
        total+=1
        '''
    print(file=sys.stdout)

if __name__=="__main__":
    # read model file
    modelfile_name=sys.argv[1]
    with open(modelfile_name,'r',encoding='utf-8') as fr:
        old_weights,labels=json.load(fr)
        weights=defaultdict(lambda:defaultdict(int))
        for key in old_weights:
            for key2 in old_weights[key]:
                weights[key][key2]=old_weights[key][key2]
        labels=set(labels)

    # read test_file
    sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
    with open("ner.output.txt",'w') as fw:
        for line in sys.stdin:
            predicate_sentence(line,labels,fw)
    '''
    print('accuracy:',1-error/float(total),file=sys.stderr)
    '''