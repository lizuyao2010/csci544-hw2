#!/usr/bin/env bash
python3 nelearn.py ner.esp.train ner.model ner.esp.dev
cat ner.esp.dev | python3 netag.py ner.model  > test.ner.out
./conlleval.pl < ner.output.txt