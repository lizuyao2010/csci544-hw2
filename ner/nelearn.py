#!/usr/bin/env python3

#python3 nelearn.py ner.esp.train ner.model -d ner.esp.dev
import sys,json,math,random,argparse,re,copy
from collections import defaultdict




def add(weights,label,features,sign):
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        weights[feature][label]+=sign

def product(weights,label,features):    
    sum_val=0
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        sum_val+=weights[feature][label]
    return sum_val

def add_weights(averaged_weights,weights,features,n):
    for feature in features:
        if feature not in averaged_weights:
            averaged_weights[feature]=defaultdict(float)

        for label in labels:
            averaged_weights[feature][label]+=weights[feature][label]*n

def normalize_weights(averaged_weights,n):
    for feature in averaged_weights:
        for label in weights[feature]:
            averaged_weights[feature][label]/=float(n)

def argmax_dot_product(features,weights,labels):
    max_label=""
    max_val=float('-infinity')
    for label in labels:
        val=product(weights,label,features)
        if val>max_val:
            max_label=label
            max_val=val
    return max_label

def get_feature(s):
    feature=[]
    for ch in s:
        if ch.islower(): 
            if 'a' not in feature:
                feature.append('a')
        elif ch.isupper():
            if 'A' not in feature:
                feature.append('A')
        elif ch.isdigit():
            if '9' not in feature:
                feature.append('9')
        else:
            if '-' not in feature:
                feature.append('-')
    return ''.join(feature)

def create_features(line,train_examples,labels):
    # parse sentence
    sentence=line.strip().split()
    sentence.insert(0,'<bos>/<bos>/<bos>')
    sentence.insert(0,'<bos>/<bos>/<bos>')
    sentence.append('<eos>/<eos>/<eos>')
    n=len(sentence)
    for (i,triple) in enumerate(sentence[2:n-1],2):
        word,tag,ner=re.match(r"(\S+)?/(\S+)/(\S+)",triple).groups('')
        p2_word,p2_tag,p2_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-2]).groups('')
        p1_word,p1_tag,p1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i-1]).groups('')
        n1_word,n1_tag,n1_ner=re.match(r"(\S+)?/(\S+)/(\S+)",sentence[i+1]).groups('')
        if ner not in labels:
            labels.add(ner)
        example=[ner,'1_'+p1_ner,'2_'+p1_tag,'3_'+word,'4_'+tag,'5_'+n1_word,'6_'+n1_tag,'8_'+p1_word,'12_'+p2_tag,'13_'+p2_ner]
        if word:
            example.append('7_'+get_feature(word))
        if len(word)>0:
            example.append('11_'+word[-1:])
            if len(word)>1:
                example.append('9_'+word[-2:])
                if len(word)>2:
                    example.append('10_'+word[-3:])
        if len(word)>0:
            example.append('14_'+word[:1])
            if len(word)>1:
                example.append('15_'+word[:2])
                if len(word)>2:
                    example.append('16_'+word[:3])
        train_examples.append(example)


def train(examples):
    global n
    global c
    error=0
    total=len(examples)
    for (i,example) in enumerate(examples):
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,weights,labels)
        if label!=pre_label:
            error+=1
            add(weights,label,features,1)
            add(weights,pre_label,features,-1)
            add(averaged_weights,label,features,n*total-c)
            add(averaged_weights,pre_label,features,-(n*total-c))
        #add_weights(averaged_weights,weights,features,total-i)
        c+=1
    error_rate=float(error)/float(total)
    return error_rate    

def predicate(examples):
    error=0
    total=len(examples)
    for example in examples:
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,averaged_weights,labels)
        if label!=pre_label:
            error+=1
    error_rate=float(error)/float(total)
    return error_rate 

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("TRAININGFILE", help="provide TRAININGFILE")
    parser.add_argument("MODEL", help="provide MODEL")
    parser.add_argument("-d","--DEVFILE", help="provide DEVFILE")
    args = parser.parse_args()
    
    weights=dict()
    averaged_weights=dict()
    labels=set()
    
    # read train_file
    train_examples=[]
    trainfile_name=args.TRAININGFILE
    with open(trainfile_name,'r',encoding='utf-8',errors='ignore') as fp:
        for line in fp:
            create_features(line,train_examples,labels)

    # read dev_file
    dev_examples=[]
    if args.DEVFILE:
        dev_file=args.DEVFILE
        with open(dev_file,'r',encoding='utf-8',errors='ignore') as fp:
            for line in fp:
                create_features(line,dev_examples,labels)
    
    #random.shuffle(train_examples)
    n=40
    pre_error_rate=1
    best_weights=dict()
    best_error_rate=1
    c=0
    for i in range(n):
        random.shuffle(train_examples)
        print("number of iteration:",i+1,file=sys.stderr)
        train_error_rate=train(train_examples)
        print("train accuracy:",1-train_error_rate,file=sys.stderr)
        if len(dev_examples)!=0:
            dev_error_rate=predicate(dev_examples)
            print("dev accuracy:",1-dev_error_rate,file=sys.stderr)
            if i==0:
                best_weights=copy.deepcopy(averaged_weights)
                best_error_rate=dev_error_rate
            else:
                if best_error_rate>dev_error_rate:
                    best_error_rate=dev_error_rate
                    best_weights=copy.deepcopy(averaged_weights)
    if len(dev_examples)==0:
        best_weights=copy.deepcopy(averaged_weights)
  


    modelfile_name=args.MODEL
    with open(modelfile_name,'w') as fw:
        json.dump([best_weights,list(labels)],fw)
