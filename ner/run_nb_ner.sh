#!/usr/bin/env bash
python3 create_train.py nb_train.txt > nb_train.json
python3 create_train.py test_file > nb_dev.json
python3 nblearn.py nb_train.json ner.nb
python3 nbclassify.py ner.nb nb_dev.json > nb_ner.out
cat ner.output.txt | tr ' ' '\t' |cut -f 1-3 > ner.gold
paste ner.gold nb_ner.out | tr '\t' ' ' > nb_ner.output.txt
./conlleval.pl < nb_ner.output.txt