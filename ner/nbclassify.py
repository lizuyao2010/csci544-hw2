#!/usr/bin/env python3
import json,sys,math
from collections import defaultdict

# predicate label according to log(posterior) = log(prior)+sum(log(likelihood))
# word_vector is list of words with duplicates
def predicate_label(word_vector):
    maxpair=["NULL",float('-infinity')]
    for label in prior_dist:
        sum_value=math.log(prior_dist[label])
        for word in word_vector:
            if word in likelihood[label]:
                sum_value+=math.log(likelihood[label][word])
            else:
                sum_value+=math.log(1.0/total_count[label])
        if sum_value>maxpair[1]:
            maxpair=[label,sum_value]
    return maxpair[0]

# read model file
modelfile_name=sys.argv[1]
with open(modelfile_name,'r',encoding='utf-8') as fr:
    likelihood,total_count,prior_dist=json.load(fr)
# read test file, label each line
testfile_name=sys.argv[2]
prefix=testfile_name.split('_')[0]
goldfile=prefix+"_"+"goldfile"
with open(testfile_name,'r',encoding='utf-8') as fp, open(goldfile,'w') as fw:
    for line in fp:
        line=line.strip()
        correct_label,word_vector=json.loads(line)
        print(correct_label,file=fw)
        print(predicate_label(word_vector))   

