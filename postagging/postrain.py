#!/usr/bin/env python3

#python3 postrain.py pos.train pos.per -d pos.dev
import sys,json,math,random,argparse,copy
from collections import defaultdict

def add(weights,label,features,sign):
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        weights[feature][label]+=sign

def product(weights,label,features):    
    sum_val=0
    for feature in features:
        if feature not in weights:
            weights[feature]=defaultdict(int)
        sum_val+=weights[feature][label]
    return sum_val

def add_weights(averaged_weights,weights,features,labels,n):
    for feature in features:
        if feature not in averaged_weights:
            averaged_weights[feature]=defaultdict(int)

        for label in labels:
            averaged_weights[feature][label]+=weights[feature][label]*n


def normalize_weights(averaged_weights,n):
    for feature in averaged_weights:
        for label in weights[feature]:
            averaged_weights[feature][label]/=float(n)

def argmax_dot_product(features,weights,labels):
    max_label=""
    max_val=float('-infinity')
    for label in labels:
        val=product(weights,label,features)
        if val>max_val:
            max_label=label
            max_val=val
    return max_label

def get_feature(s):
    feature=[]
    for ch in s:
        if ch.islower(): 
            if 'a' not in feature:
                feature.append('a')
        elif ch.isupper():
            if 'A' not in feature:
                feature.append('A')
        elif ch.isdigit():
            if '9' not in feature:
                feature.append('9')
        else:
            if '-' not in feature:
                feature.append('-')
    return ''.join(feature)


def create_features(line,train_examples,labels):
    # parse sentence
    sentence=line.strip().split()
    sentence.insert(0,'<bos>/<bos>')
    sentence.insert(0,'<bos>/<bos>')
    sentence.append('<eos>/<eos>')
    n=len(sentence)
    for (i,pair) in enumerate(sentence[2:n-1],2):
        word,tag=pair.split('/')
        f2_word,f2_tag=sentence[i-2].split('/')
        f_word,f_tag=sentence[i-1].split('/')
        t_word,t_tag=sentence[i+1].split('/')
        if tag not in labels:
            labels.add(tag)
        example=[tag,'1_'+f_tag,'2_'+word,'3_'+t_word,'4_'+f2_tag,'6_'+f_word]
        example.append('5_'+get_feature(word))
        
        if len(word)>1:
            example.append('7_'+word[-2:])
            if len(word)>2:
                example.append('8_'+word[-3:])
        
        train_examples.append(example)


def train(examples):
    global n
    global c
    error=0
    total=len(examples)
    for (i,example) in enumerate(examples):
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,weights,labels)
        if label!=pre_label:
            error+=1
            add(weights,label,features,1)
            add(weights,pre_label,features,-1)
            add(averaged_weights,label,features,n*total-c)
            add(averaged_weights,pre_label,features,-(n*total-c))
        c+=1
        #add_weights(averaged_weights,weights,features,labels,total-i+total*remain)
    error_rate=float(error)/float(total)
    return error_rate    

def predicate(examples):
    error=0
    total=len(examples)
    for example in examples:
        label=example[0]
        features=example[1:]
        pre_label=argmax_dot_product(features,averaged_weights,labels)
        if label!=pre_label:
            error+=1
    error_rate=float(error)/float(total)
    return error_rate 

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("TRAININGFILE", help="provide TRAININGFILE")
    parser.add_argument("MODEL", help="provide MODEL")
    parser.add_argument("-d","--DEVFILE", help="provide DEVFILE")
    args = parser.parse_args()

    weights=dict()
    averaged_weights=dict()
    labels=set()
    
    # read train_file
    train_examples=[]
    trainfile_name=args.TRAININGFILE
    with open(trainfile_name,'r',encoding='utf-8') as fp:
        for line in fp:
            create_features(line,train_examples,labels)

    
    # read dev_file
    dev_examples=[]
    if args.DEVFILE:
        dev_file=args.DEVFILE
        with open(dev_file,'r',encoding='utf-8') as fp:
            for line in fp:
                create_features(line,dev_examples,labels)
    
    
    n=20
    pre_error_rate=1
    best_weights=dict()
    best_error_rate=1  
    c=0
    for i in range(n):
        random.shuffle(train_examples)
        print("number of iteration:",i+1,file=sys.stderr)
        train_error_rate=train(train_examples)
        print("train accuracy:",1-train_error_rate,file=sys.stderr)
        if len(dev_examples)!=0:
            dev_error_rate=predicate(dev_examples)
            print("dev accuracy:",1-dev_error_rate,file=sys.stderr)
        
            if i==0:
                best_weights=copy.deepcopy(averaged_weights)
                best_error_rate=dev_error_rate
            else:
                if best_error_rate>dev_error_rate:
                    best_error_rate=dev_error_rate
                    best_weights=copy.deepcopy(averaged_weights)

    if len(dev_examples)==0:
        best_weights=copy.deepcopy(averaged_weights)
        

    modelfile_name=args.MODEL
    with open(modelfile_name,'w') as fw:
        json.dump([best_weights,list(labels)],fw)
