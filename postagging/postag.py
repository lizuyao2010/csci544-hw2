#!/usr/bin/env python3
#cat pos.dev | python3 postag.py pos.per > pos.dev.out
#cat pos.test | python3 postag.py pos.per > pos.test.out
import json,sys
from postrain import add,product,argmax_dot_product,get_feature
from collections import defaultdict

total=0
error=0

def predicate_sentence(line,labels):
    # parse sentence
    sentence=line.strip().split()
    sentence.insert(0,'<bos>/<bos>')
    sentence.insert(0,'<bos>/<bos>')
    sentence.append('<eos>/<eos>')
    n=len(sentence)
    f_tag='<bos>'
    f2_tag='<bos>'
    for (i,pair) in enumerate(sentence[2:n-1],2):
        word=pair
        #f2_word,f2_tag=sentence[i-2].split('/')
        #f_word,f_tag=sentence[i-1].split('/')
        f2_word=sentence[i-2]#.split('/')[0]
        f_word=sentence[i-1]#.split('/')[0]
        t_word=sentence[i+1]#.split('/')
        features=['1_'+f_tag,'2_'+word,'3_'+t_word,'4_'+f2_tag,'6_'+f_word]
        features.append('5_'+get_feature(word))
        
        if len(word)>1:
            features.append('7_'+word[-2:])
            if len(word)>2:
                features.append('8_'+word[-3:])

        # predicate label
        pre_tag=argmax_dot_product(features,weights,labels)
        f2_tag=f_tag
        f_tag=pre_tag
        print(word+'/'+pre_tag,end=' ',file=sys.stdout)
        '''
        if tag!=pre_tag:
            global error
            error+=1
        global total
        total+=1
        '''
    print(file=sys.stdout)

if __name__=="__main__":
    # read model file
    modelfile_name=sys.argv[1]
    with open(modelfile_name,'r',encoding='utf-8') as fr:
        old_weights,labels=json.load(fr)
        weights=defaultdict(lambda:defaultdict(int))
        for key in old_weights:
            for key2 in old_weights[key]:
                weights[key][key2]=old_weights[key][key2]
        labels=set(labels)

    # read test_file
    for line in sys.stdin:
        predicate_sentence(line,labels)
    '''
    print('accuracy:',1-error/float(total),file=sys.stderr)
    '''
