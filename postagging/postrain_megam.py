#!/usr/bin/env python3

#python3 postrain_megam.py pos.train pos.model
import sys,os

# read train_file
trainfile_name=sys.argv[1]
with open(trainfile_name,'r',encoding='utf-8') as fp, open('megam_train.txt','w',encoding='utf-8') as fw:
    for line in fp:
        # parse sentence
        sentence=line.strip().split()
        sentence.insert(0,'<bos>/<bos>')
        sentence.append('<eos>/<eos>')
        n=len(sentence)
        for (i,pair) in enumerate(sentence[1:n-1],1):
            word,tag=pair.split('/')
            f_word,f_tag=sentence[i-1].split('/')
            t_word,t_tag=sentence[i+1].split('/')
            print(tag,'1_'+f_word,'2_'+word,'3_'+t_word,file=fw)
model=sys.argv[2]
os.system("./megam.opt -nc multitron megam_train.txt > "+model)