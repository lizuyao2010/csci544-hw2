#!/usr/bin/env python3

#cat pos.dev | python3 postag_megam.py pos.model > test.pos.out

import sys,os
words=[]
newlines=set()
with open('test_file','w',encoding='utf-8') as fw:
    index=-1
    for line in sys.stdin:
        line=line.strip()
        if '/' not in line:
            sentence=[word+'/<unknown>' for word in line.split()]
        else:
            sentence=line.split()
        sentence.insert(0,'<bos>/<bos>')
        sentence.append('<eos>/<eos>')
        n=len(sentence)
        for (i,pair) in enumerate(sentence[1:n-1],1):
            word,tag=pair.split('/')
            f_word,f_tag=sentence[i-1].split('/')
            t_word,t_tag=sentence[i+1].split('/')
            print(tag,'1_'+f_word,'2_'+word,'3_'+t_word,file=fw)
            words.append(word)
            index+=1
        newlines.add(index)
os.system('./megam.opt -nc -predict '+sys.argv[1]+' multitron test_file >test.out')
with open('test.out','r',encoding='utf-8') as fr:
    for (i,predicate) in enumerate(fr):
        predicate=predicate.strip().split()
        pre_tag=predicate[0]
        print(words[i]+'/'+pre_tag,end=' ',file=sys.stdout)
        if i in newlines:
            print(file=sys.stdout)